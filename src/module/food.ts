// 定义一个食物类
class Food {
    // 食物元素
    foodElement: HTMLElement;

    constructor() {
        this.foodElement = document.getElementById('food')!;
    }
    // 食物的x轴坐标
    get X() {
        return this.foodElement.offsetLeft;
    }
    // 食物的Y轴坐标
    get Y() {
        return this.foodElement.offsetTop;
    }
    // 定义一个方法来改变食物的位置
    changeFoodPostion() {
        // 蛇的一格是10 那么每次移动10 食物也是10
        this.foodElement.style.left = Math.round(Math.random() * 29) * 10 + 'px';
        this.foodElement.style.top = Math.round(Math.random() * 29) * 10 + 'px';
    }
}
export default Food;