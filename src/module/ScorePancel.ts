// 定义一个计分板的类
class ScorePancel {
    // 分数元素
    scoreElement: HTMLElement;
    // 等级元素
    levelElement: HTMLElement;
    // 分数
    score: number = 0;
    // 等级
    level: number = 1;
    // 最大多少级
    maxLevel: number;
    // 多少分升一级
    upScore: number;
    constructor(maxLevel: number = 10, upScore: number = 10) {
        this.scoreElement = document.getElementById('score')!;
        this.levelElement = document.getElementById('level')!;
        this.maxLevel = maxLevel;
        this.upScore = upScore;
    }
    // 增加分数
    addScore() {
        // 增加分数
        this.scoreElement.innerHTML = `${++this.score}`;
        // 是否达到升一级的分数
        if (this.score % this.upScore === 0) {
            this.levelUp();
        }
    }
    // 等级增加一级
    levelUp() {
        this.levelElement.innerHTML = `${++this.level}`;
    }
}
export default ScorePancel;