// 引入一个包,这个实际就是nodejs里面的一个模块，作用就是用来帮助我们拼接一些路径
const path = require('path');
// 引入html插件
const HTMLWebpackPlugin = require('html-webpack-plugin')
// 引入clean插件
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
module.exports = {

    //指定入口文件
    entry: './src/index.ts',
    //指定打包后的文件所在的目录
    output: {
        //指定打包文件的目录,指定dist目录
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        environment:{
        //设置webpack不使用箭头
            arrowFunction:false,
            // 不使用const
        const:false
        },
        
    },
    //指定webpack打包时候使用的模块
    module: {
        rules: [
            {
                // test指定的是规则生效的文件
                test: /\.ts$/,
                //要使用的loader
                use: [
                    {
                        loader: 'babel-loader',
                        // 设置babel
                        options:{
                            // 设置定义的环境
                        presets: [
                            [
                                // 指定环境插件
                                "@babel/preset-env",
                                {
                                    // 配置环境，兼容的浏览器版本
                                    targets: {
                                        "chrome": "79",
                                        "ie": "11"
                                    },

                                    "corejs": "3", // 跟安装的版本一直
                                    // 使用core.js的方式，，usage 按需健在
                                    "useBuiltIns": "usage" //
                                }
                            ]
                        ]
                        }
                    },
                    'ts-loader'
                ],
                //要排除的文件
                exclude: /node-modules/
            },
              // 设置less文件的处理
              {
                test:/\.less$/,
                // less-loader less的加载器 css-loader 处理css代码 style-loader 把css引入到style里面
                use:[
                    "style-loader",
                    "css-loader",
                    {
                    loader:"postcss-loader",
                    options:{
                        postcssOptions:{
                            plugins:[
                               [
                                "postcss-preset-env",
                                {
                                    browsers:"last 3 versions"
                                }
                               ]
                            ]
                        }
                    }
                    },
                    "less-loader"
                ]
            },

        ]
    },
    // 配置webpack插件
    plugins: [
        new HTMLWebpackPlugin({
            // title:'我是一个已定义的title',
            template: './src/index.html'
        }),
        new CleanWebpackPlugin()
    ],
    // 用来设置引用模块
    resolve: {
        extensions: ['.ts', '.js']
    }
};